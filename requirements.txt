# Core components
flask
flask_restful
pony
pyyaml

# General utilities
numpy
scipy

# summary plot generation
matplotlib
seaborn

# PDF generation
pdfrw
reportlab
Wand
Pillow  # also scan processing
pyStrich  # TODO: can we replace this with stuff from pylibdmtx?

# Scan processing
opencv-python
git+https://github.com/mstamy2/PyPDF2
pylibdmtx

# Exporting
pandas
openpyxl  # required for writing dataframes as Excel spreadsheets
